#include "Div.h"
#include "Mul.h"
#include "Sub.h"
#include <sstream>

Div::Div(Expression *firstExpression, Expression *secondExpression) {
	this->firstExpression = firstExpression;
	this->secondExpression = secondExpression;
}

Div::~Div() {
	delete firstExpression;
	delete secondExpression;
}

Expression *Div::diff() const {
	Expression *subExpression = new Sub(new Mul(firstExpression->diff(), secondExpression->copy()), new Mul(firstExpression->copy(), secondExpression->diff()));
	Expression *sqrExpression = new Mul(secondExpression->copy(), secondExpression->copy());

	return new Div(subExpression, sqrExpression);
}

std::string Div::string() {
	std::stringstream result;

	result << "(";
	result << firstExpression->string();
	result << " / ";
	result << secondExpression->string();
	result << ")";

	return result.str();
}

Expression *Div::copy() const {
	return new Div(firstExpression->copy(), secondExpression->copy());
}