#ifndef DIV_H
#define DIV_H

#include "Expression.h"

class Div : public Expression {
public:
	Div(Expression *firstExpression, Expression *secondExpression);
	~Div();

	Expression *diff() const;
	std::string string();
	Expression *copy() const;
private:
	Expression *firstExpression;
	Expression *secondExpression;
};

#endif