#ifndef EXPRESSION_H
#define EXPRESSION_H
#include <string>

class Expression {
public:
	virtual ~Expression();

	virtual Expression *diff() const = 0;
	virtual std::string string() = 0;
	virtual Expression *copy() const = 0;
};

#endif