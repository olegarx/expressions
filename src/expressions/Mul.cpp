#include "Mul.h"
#include "Sum.h"
#include <sstream>

Mul::Mul(Expression *firstExpression, Expression *secondExpression) {
	this->firstExpression = firstExpression;
	this->secondExpression = secondExpression;
}

Mul::~Mul() {
	delete firstExpression;
	delete secondExpression;
}

Expression *Mul::diff() const {
	return new Sum(new Mul(firstExpression->diff(), secondExpression->copy()), new Mul(firstExpression->copy(), secondExpression->diff()));
}

std::string Mul::string() {
	std::stringstream result;

	result << "(";
	result << firstExpression->string();
	result << " * ";
	result << secondExpression->string();
	result << ")";

	return result.str();
}

Expression *Mul::copy() const {
	return new Mul(firstExpression->copy(), secondExpression->copy());
}