#ifndef MUL_H
#define MUL_H

#include "Expression.h"

class Mul : public Expression {
public:
	Mul(Expression *firstExpression, Expression *secondExpression);
	~Mul();

	Expression *diff() const;
	std::string string();
	Expression *copy() const;
private:
	Expression *firstExpression;
	Expression *secondExpression;
};

#endif