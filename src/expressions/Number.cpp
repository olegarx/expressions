#include "Number.h"
#include <sstream>

Number::Number(double number) {
	this->number = number;
}

Expression *Number::diff() const {
	return new Number(0);
}
std::string Number::string() {
	std::stringstream result;
	result << number;
	return result.str();
}
Expression *Number::copy() const {
	return new Number(number);
}