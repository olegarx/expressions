#ifndef NUMBER_H
#define NUMBER_H

#include "Expression.h"

class Number : public Expression {
public:
	Number(double number);

	Expression *diff() const;
	std::string string();
	Expression *copy() const;
private:
	double number;
};

#endif