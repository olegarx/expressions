#include "Sub.h"
#include <sstream>

Sub::Sub(Expression *firstExpression, Expression *secondExpression) {
	this->firstExpression = firstExpression;
	this->secondExpression = secondExpression;
}

Sub::~Sub() {
	delete firstExpression;
	delete secondExpression;
}

Expression *Sub::diff() const {
	return new Sub(firstExpression->diff(), secondExpression->diff());
}

std::string Sub::string() {
	std::stringstream result;

	result << "(";
	result << firstExpression->string();
	result << " - ";
	result << secondExpression->string();
	result << ")";

	return result.str();
}
Expression *Sub::copy() const {
	return new Sub(firstExpression->copy(), secondExpression->copy());
}