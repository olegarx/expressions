#ifndef SUB_H
#define SUB_H

#include "Expression.h"

class Sub : public Expression {
public:
	Sub(Expression *firstExpression, Expression *secondExpression);
	~Sub();

	Expression *diff() const;
	std::string string();
	Expression *copy() const;
private:
	Expression *firstExpression;
	Expression *secondExpression;
};

#endif