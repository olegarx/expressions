#include "Sum.h"
#include <sstream>

Sum::Sum(Expression *firstExpression, Expression *secondExpression) {
	this->firstExpression = firstExpression;
	this->secondExpression = secondExpression;
}

Sum::~Sum() {
	delete firstExpression;
	delete secondExpression;
}

Expression *Sum::diff() const {
	return new Sum(firstExpression->diff(), secondExpression->diff());
}

std::string Sum::string() {
	std::stringstream result;

	result << "(";
	result << firstExpression->string();
	result << " + ";
	result << secondExpression->string();
	result << ")";

	return result.str();
}

Expression *Sum::copy() const {
	return new Sum(firstExpression->copy(), secondExpression->copy());
}