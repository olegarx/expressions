#ifndef SUM_H
#define SUM_H

#include "Expression.h"

class Sum : public Expression {
public:
	Sum(Expression *firstExpression, Expression *secondExpression);
	~Sum();

	Expression *diff() const;
	std::string string();
	Expression *copy() const;
private:
	Expression *firstExpression;
	Expression *secondExpression;
};

#endif