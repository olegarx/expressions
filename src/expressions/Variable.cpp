#include "Variable.h"
#include "Number.h"

Variable::Variable() {
}

Expression *Variable::diff() const {
	return new Number(1);
}
std::string Variable::string() {
	return std::string("x");
}
Expression *Variable::copy() const {
	return new Variable();
}