#ifndef VARIABLE_H
#define VARIABLE_H

#include "Expression.h"

class Variable : public Expression {
public:
	Variable();

	Expression *diff() const;
	std::string string();
	Expression *copy() const;
private:
};

#endif