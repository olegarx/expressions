#include <iostream>
#include "expressions/Number.h"
#include "expressions/Variable.h"
#include "expressions/Sum.h"

int main() {
	Expression *exp = new Sum(new Variable(), new Number(1));
	std::cout << exp->string() << std::endl;
	Expression *diff = exp->diff();
	std::cout << diff->string() << std::endl;

	delete diff;
	delete exp;

	return 0;
}